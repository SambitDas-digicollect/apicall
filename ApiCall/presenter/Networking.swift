//
//  Networking.swift
//  ApiCall
//
//  Created by Sambit Das on 14/04/20.
//  Copyright © 2020 Sambit Das. All rights reserved.
//

import Foundation
import Alamofire

class Networking {
    
    static var url = "https://my.api.mockaroo.com/data.json?key=91fc6070"
    
    public static func fetchData(onCompletion: @escaping ([User]?, Bool) -> Void) -> Void {
    
        
        AF.request(url).response { response in
           // debugPrint(response)
            switch response.result {
            case .success :
                guard let data = response.data else { return }
                do {
                    let userList = try JSONDecoder().decode([User].self, from: data)
                    onCompletion(userList, true)
                } catch {
                    print(error.localizedDescription)
                    onCompletion(nil, false)
                }
                break
                
            case .failure :
                print("failed")
                onCompletion(nil, false)
                break
            }
            
        }
    }
}
