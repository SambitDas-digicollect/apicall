//
//  ViewController.swift
//  ApiCall
//
//  Created by Sambit Das on 14/04/20.
//  Copyright © 2020 Sambit Das. All rights reserved.
//

import UIKit
import Alamofire


class TableViewController: UIViewController ,UITableViewDataSource, UITableViewDelegate{
   
    var userList = [User]()
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityIndicator.hidesWhenStopped = true
        activityIndicator.startAnimating()
        
        Networking.fetchData() {[weak self] (list, isSuccess) in
            guard let weakSelf = self else { return }
            if let value = list, isSuccess == true {
                weakSelf.userList = value
                weakSelf.tableView.reloadData()
                weakSelf.activityIndicator.stopAnimating()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        userList.count
    }
       
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! TableViewCell
            cell.firstNameLabel.text = " first name : \(userList[indexPath.row].firstName)"
            cell.lastNameLabel.text = " last name : \(userList[indexPath.row].lastName)"
            cell.genderLabel.text = " gender : \(userList[indexPath.row].gender)"
            cell.idLabel.text = " id : \(String(userList[indexPath.row].id))"
            cell.emailLabel.text = " email : \(userList[indexPath.row].email)"
            return cell
       }
}


