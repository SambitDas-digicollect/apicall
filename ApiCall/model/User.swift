//
//  User.swift
//  ApiCall
//
//  Created by Sambit Das on 14/04/20.
//  Copyright © 2020 Sambit Das. All rights reserved.
//

import Foundation

struct User: Codable {
    let email, lastName: String
    let id: Int
    let firstName: String
    let gender: String

    enum CodingKeys: String, CodingKey {
        case email
        case lastName = "last_name"
        case id
        case firstName = "first_name"
        case gender
    }
}
enum Gender: String, Codable {
    case female = "Female"
    case male = "Male"
}
